'use strict';

var functions = require('firebase-functions');
// The Firebase Admin SDK to access the Firebase Realtime Database. 
// var admin = require('firebase-admin');
// //admin.initializeApp(functions.config().firebase);

// admin.initializeApp();

var admin = require("firebase-admin");

//var serviceAccount = require("/Users/Srini/dev/firebase/animodraw/movindraw-firebase-adminsdk-nvz10-9f99b09707.json");

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'https://movindraw.firebaseio.com'
});

// functions.database.ref('/drawings');
// functions.database.instance('movindraw').ref('/drawings');

// Keeps track of the length of the 'likes' child list in a separate property.
exports.countdrawingschange = functions.database.ref('/drawings/{d_id}').onWrite(event => {

  console.log("cloud function - countdrawingschange triggered");
  console.log(event.data);
  
  const collectionRef = event.data.ref.parent;
  console.log(collectionRef);

  const countRef = collectionRef.parent.child('stats/drawings_count');
  console.log(collectionRef);
  
  // Return the promise from countRef.transaction() so our function 
  // waits for this async event to complete before it exits.
  return countRef.transaction(current => {
    if (event.data.exists() && !event.data.previous.exists()) {
      return (current || 0) + 1;
    }
    else if (!event.data.exists() && event.data.previous.exists()) {
      return (current || 0) - 1;
    }
  }).then(() => {
    console.log('Counter updated.');
  });
});


// If the number of likes gets deleted, recount the number of likes
exports.recountDrawings = functions.database.ref('/stats/drawings_count').onWrite(event => {
  if (!event.data.exists()) {
    const counterRef = event.data.ref.parent;
    const collectionRef = counterRef.parent.child('drawings');
    
    // Return the promise from counterRef.set() so our function 
    // waits for this async event to complete before it exits.
    return collectionRef.once('value')
        .then(messagesData => counterRef.child('drawings_count').set(messagesData.numChildren()));
  }
});


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
