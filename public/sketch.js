var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    wx = w.innerWidth || e.clientWidth || g.clientWidth,
    wy = w.innerHeight|| e.clientHeight|| g.clientHeight;
//alert(x + ' × ' + y);

//var db = database;
var input, button, greeting;
var btn_new, btn_chng_clrs, btn_save, btn_show, btn_overlap;
var slider_stroke_size;
var btn_new_anim;
var movies_select;
var drawings_select;
var isPlayMovie;

var drawing_path_len;
var movie_path_len;
var cc_bkg;
var cc_stroke;
var dd_stroke;
var cc_stroke_multi1;
var distort;
var changes;
var dx;
var dy;
var multi_render_offset1;
var is_overlap;
var is_matisse;
var is_distort;
var cc_len = 200;


//point class
function Point(pmouseX, pmouseY, mouseX, mouseY)
{
  this.px = pmouseX;
  this.py = pmouseY;
  this.cx = mouseX;
  this.cy = mouseY;

  this.incrementPx = function(dx){
    this.px = this.px + dx;
  };
  this.incrementPy = function(dy){
    this.py = this.py + dy;
  };
  this.incrementCx = function(dx){
    this.cx = this.cx + dx;
  };
  this.incrementCy = function(dy){
    this.cy = this.cy + dy;
  };

  this.render = function()
  {
    //draw the figure
    line(this.px, this.py, this.cx, this.cy);
  };

  this.shimmer = function()
  {
    //draw the figure
    line(this.px+random(-3,3), this.py+random(-3,3), this.cx+random(-3,3), this.cy+random(-3,3));
  };
}

//canvas settings
// var canvas = document.getElementById('myCanvas');
// var context = canvas.getContext('2d');

function setup() {
  // context.canvas.width  = wx;
  // context.canvas.height = wy;
  canvas = createCanvas(wx, wy);
  frameRate(100);
  
  // button = createButton('submit');
  // button.position(input.x + input.width, 65);
  // button.mousePressed(greet);
  textSize(10);

  btn_new = createButton('make a drawing');
  btn_new.position(20, 20);
  //btn_new.class("noselect");
  btn_new.class("noselect createbtn");
  btn_new.mousePressed(newDrawing);


  btn_chng_clrs = createButton('change colours');
  btn_chng_clrs.position(btn_new.x + btn_new.width + 5, 20);
  btn_chng_clrs.class("noselect");
  btn_chng_clrs.mousePressed(changeColours);

  slider_stroke_size = createSlider(1,50,5);
  slider_stroke_size.class("noselect");
  slider_stroke_size.position(btn_chng_clrs.x + btn_chng_clrs.width + 5, 20);

  btn_save = createButton('save');
  btn_save.class("noselect hidethem save");
  btn_save.position(slider_stroke_size.x + slider_stroke_size.width + 5, 20);
  btn_save.mousePressed(saveDrawing);


  input = createInput();
  input.class("noselect hidethem");
  input.position(btn_save.x + btn_save.width + 5, 20);

  greeting = createElement('h0', 'describe this drawing');
  greeting.class("noselect hidethem");
  greeting.position(input.x + input.width + 5, 20);
  //textSize(50);
  textAlign(CENTER);

  // btn_autodraw = createButton('Autodraw');
  // btn_autodraw.position(greeting.x + greeting.width + 5, 20);
  // btn_autodraw.mousePressed(turnAutodrawOn);


  // btn_new_anim = createButton('new animation');
  // btn_new_anim.position(20, wy-40);
  // btn_new_anim.mousePressed(newAnimation);

  // movies_select = createSelect("moviesList");
  // movies_select.position(greeting.x + greeting.width + 5, 20);
  // movies_select.id("moviesList");
  // // movies_select.option("See previous movies!", "").value("").disabled("disabled").selected("selected");
  // movies_select.option("See previous movies!", "");
  // movies_select.changed(loadMovie);

  // drawings_select = createSelect("drawingsList");
  // drawings_select.position(movies_select.x + movies_select.width + 5, 20);
  // drawings_select.id("drawingsList");
  // // movies_select.option("See previous movies!", "").value("").disabled("disabled").selected("selected");
  // drawings_select.option("See previous drawings!", "");
  // drawings_select.changed(loadDrawing);

  //setting up the drawing
  //colorMode(HSB, 360, 100, 100);
  dpath = [];
  cc_bkg = color(255);
  cc_stroke = color(0);
  cc_stroke_multi1 = color(127);
  distort = false;
  changes = 0.0;
  dx = 0.0;
  dy = 0.0;

  isPlayMovie = false;
  is_overlap = false;
  is_matisse = false;
  is_distort = false;
  resetPathSize();
  resetMoviePathSize();

  var dd_stroke = slider_stroke_size.value();
  strokeWeight(dd_stroke);
}

function draw() {
  //-- all the settings 
    //for sequential rendering maintain a global count to decrement
    seqRender();

    //repaint all the control texts
    // strokeWeight(1);
    // textStyle(NORMAL);
    //text("line size", slider_stroke_size.x + slider_stroke_size.width, 25);    
}

function seqRender() 
{
    //background(bkg_cc);
    //strokeWeight(dd_stroke);
    if(drawing_path_len < dpath.length)
    {
      //Point p = (Point) dpath.get(drawing_path_len);
      if(is_distort)
      {
        // dpath[drawing_path_len].incrementPx(dx);
        // dpath[drawing_path_len].incrementPy(dy);

        // dx = random(-3, 3);
        // dy = random(-3, 3);
        
        // dpath[drawing_path_len].incrementCx(dx);
        // dpath[drawing_path_len].incrementCy(dy);
        if(int(random(cc_len)) == 0)
        {
          // console.log("changing stroke");
          changeStrokeSize();
        }
      }
      else
      {
         var dd_stroke = slider_stroke_size.value();
        strokeWeight(dd_stroke);
      }

      if(int(random(cc_len)) == 0 && is_matisse == true)
      {
        // console.log("changing stroke");
        changeColoursStroke();
      }
      else
      {
        if(int(random(cc_len)) == 0)
        {
          changeColoursStroke();
        }
      }
      dpath[drawing_path_len].render();
      drawing_path_len = drawing_path_len + 1;
    }
    else
    {
      
      if(is_overlap == false)
      {
        if(int(random(2))== 0){
          is_matisse=true;
          cc_bkg = color(255);
          background(cc_bkg);
          // console.log("is_matisse is true");
        }else
        {
          is_matisse=false;
          stroke(cc_stroke);
          background(cc_bkg);
        }
      }
      resetPathSize();
      if(isPlayMovie)
      {
        advanceMovieSeq();
      }
    }
}

function advanceMovieSeq() {
    //background(bkg_cc);
    //strokeWeight(dd_stroke);
    if(movie_path_len < mpath.length)
    {      
      var t = mpath[movie_path_len];
      loadMovieSequence(t.id);
      movie_path_len = movie_path_len + 1;
    }
    else
    {
      stroke(cc_stroke);
      background(cc_bkg);
      var t = mpath[0];
      loadMovieSequence(t.id);
      movie_path_len = 1;
    }
}

function resetPathSize()
{
    drawing_path_len = 1;
    multi_render_offset1 = dpath.length/2;
    autoDraw();
    //changeColours();
}
function resetMoviePathSize()
{
  movie_path_len = 0;
}

function mouseDragged() {
  // A new Point object is added to the ArrayList, by default to the end  
  //stroke(0);
  line(pmouseX, pmouseY, mouseX, mouseY);
  
  dpath.push(new Point(pmouseX, pmouseY, mouseX, mouseY));
  resetPathSize();
}

//---------------- Button Functions ------------------//

function newDrawing() {
  cc_bkg = color(255);
  background(cc_bkg);
  cc_stroke = color(0);
  stroke(cc_stroke);
  // stroke_multi1 = color(0);
  //changeColours();
  
  greeting.html('describe your drawing!');
  greeting.style("color", "black");
  input.value('');

  greeting.class("noselect showthem");
  btn_save.class("noselect showthem save");
  input.class("noselect showthem");
  
  newDrawingJs();
}

function changeColours() {

  if(is_overlap == false)
  {
    if(is_matisse == true)
    {
      cc_bkg = color(255);
      background(cc_bkg);
    }
    else
    {
      cc_bkg = color(random(255),random(255), random(255));
      background(cc_bkg);
      cc_stroke = color(random(255),random(255),random(255));
      stroke(cc_stroke);
      cc_stroke_multi1 = color(random(255),random(255),random(255));    
    }
  }
  else
  {
    if(is_matisse == true)
    {
    }
    else
    {
      cc_stroke = color(random(255),random(255),random(255));
      stroke(cc_stroke);
      cc_stroke_multi1 = color(random(255),random(255),random(255));    
    }

  }
}

function changeColoursStroke() {
  // cc_bkg = color(255);
  // background(cc_bkg);
  cc_stroke = color(random(255),random(255),random(255));
  stroke(cc_stroke);
  // cc_stroke_multi1 = color(random(255),random(255),random(255));
}

function changeStrokeSize()
{
  strokeWeight(random(1,50));
}
function saveDrawing() {
  //invoke firebase save
  console.log("save called");
  var name = input.value();

  if(name === undefined || name === "")
  {
    greeting.html('you need to describe the drawing to save !');
    greeting.style("color", "red");
  }
  else
  {
    saveDrawingJs(name);
    saveD();
  }
}

function saveD() {
  //invoke firebase save
  console.log("saved drawing");

  greeting.html('saved your drawing!');
  greeting.style("color", "grey");

  greeting.class("noselect hidethem");
  btn_save.class("noselect hidethem");
  input.class("noselect hidethem");
}

function done(){
  console.log("done!!");
}

function createDrawingRef(key, name){

}

// function turnAutodrawOn(){
//   isAutodraw = true;
// }
//------New Animation Section ----//

// //wiring event listeners
// var saveBtn = document.getElementById('saveD')
// saveBtn.addEventListener('click', function(evt) {
//   console.log("clicked");
// }, false);

// function greet() {
//   var name = input.value();
//   greeting.html('hello '+name+'!');
//   input.value('');

//   for (var i=0; i<200; i++) {
//     push();
//     fill(random(255), 255, 255);
//     translate(random(width), random(height));
//     rotate(random(2*PI));
//     text(name, 0, 0);
//     pop();
//   }
// }

// function writeNewPost(uid, username, picture, title, body) {
//   // A post entry.
//   var postData = {
//     author: username,
//     uid: uid,
//     body: body,
//     title: title,
//     starCount: 0,
//     authorPic: picture
//   };

//   // Get a key for a new Post.
//   var newPostKey = firebase.database().ref().child('posts').push().key;

//   // Write the new post's data simultaneously in the posts list and the user's post list.
//   var updates = {};
//   updates['/posts/' + newPostKey] = postData;
//   updates['/user-posts/' + uid + '/' + newPostKey] = postData;

//   return firebase.database().ref().update(updates);
// }